# Barely Competent

Greetings blog reader!

Thank you for reading [Debugging Your REST Service From Your Emulator/Device Using SharpProxy](https://www.barelycompetent.co.za/debugging-your-rest-service-from-your-emulator-device-using-sharpproxy/)!

I've created this repository for you to just download SharpProxy without building it yourself in Visual Studio.

You can just extract the zip file and use `SharpProxy.exe` immediately.
